import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/bio.jpg')} />
                    </View>
                    <Text style={styles.bio}>Siti zulfah Sulastri</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Program study : Teknik Informatika </Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>kelas : Pagi A </Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Agama : Islam</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>TTL : Purwakarta, 15-07-1999</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Alamat : Purwakarta</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Instagram : Djulaeee</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Facebook :-</Text>            
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#2F4F4F',
        flex:1,
        width: 500
    },

    bio:{
        textAlign:"center",
    marginTop:5,
    marginBottom:50,
    fontSize:18,
    fontFamily:"arial",
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: '#FFFFFF',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#FFFFFF',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: -20,
        left: 80
    },
    study:{
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    class: {
        bottom:-20,
        left: 80
    },
    kelas:{
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    Agama: {
        bottom : -20,
        left : 80
    },
    Agm: {
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    TempatTglLahir: {
        bottom : -20,
        left : 80
    },
    Ttl: {
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    Alamat: {
        bottom : -20,
        left : 80
    },
    Almt: {
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    
    instagram: {
        bottom : -20,
        left : 80
    },
    ig: {
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    facebook:{
        bottom: -20,
        left : 80
    },
    fb: {
        justifyContent: 'flex-end',
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: 'bold',
    },
    state : {
        fontSize : 25,
        left : 120,
        bottom :-15
    }
});