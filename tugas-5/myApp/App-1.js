import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, ImageBackground } from 'react-native';

import Login from './login';
import Biodata from './Biodata';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
            <ImageBackground source={require("./assets/login.jpg")} style={styles.bgcont}>
        <StatusBar
          backgroundColor='#A0522D'
          barstyle='light-content'
          />
          <Login />  
</ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bgcont:{
    flex:1,
    resizeMode:'cover',
    justifyContent:'center',
    width:'100%',
    height:'100%',
    alignItems:'center',
  },
});
